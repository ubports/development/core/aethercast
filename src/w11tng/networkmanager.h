/*
 * Copyright (C) 2015 Canonical, Ltd.
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3, as published
 * by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranties of
 * MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef W11TNG_NETWORKMANAGER_H_
#define W11TNG_NETWORKMANAGER_H_

#include <mutex>
#include <vector>

#include <NetworkManager.h>

#include <ac/networkmanager.h>

#include "managerstub.h"
#include "wififirmwareloader.h"
#include "informationelement.h"
#include "hostname1stub.h"
#include "rfkillmanager.h"

namespace w11tng {

class NetworkManagerDevice : public ac::NetworkDevice
{
public:
    typedef std::shared_ptr<w11tng::NetworkManagerDevice> Ptr;

    static NetworkManagerDevice::Ptr Create(NMDevice* device, NMWifiP2PPeer* peer);
    NetworkManagerDevice(NMDevice* device, NMWifiP2PPeer* peer);
    ~NetworkManagerDevice();

    virtual ac::MacAddress Address() const override;
    virtual ac::IpV4Address IPv4Address() const override;
    virtual std::string Name() const override;
    virtual ac::NetworkDeviceState State() const override;
    virtual std::vector<ac::NetworkDeviceRole> SupportedRoles() const override;

    NMDevice* NmDevice() const;
    NMWifiP2PPeer* NmPeer() const;
    void SetState(ac::NetworkDeviceState value);

private:
    NMDevice* nmdevice_;
    NMWifiP2PPeer* nmpeer_;
    ac::NetworkDeviceState state_;
};

class NetworkManager : public std::enable_shared_from_this<NetworkManager>,
        public ac::NetworkManager,
        public w11tng::WiFiFirmwareLoader::Delegate,
        public w11tng::ManagerStub::Delegate,
        public w11tng::Hostname1Stub::Delegate,
        public w11tng::RfkillManager::Delegate {
public:
    static constexpr const char *kBusName{"org.freedesktop.NetworkManager"};
    static ac::NetworkManager::Ptr Create();

    ~NetworkManager();

    void SetDelegate(ac::NetworkManager::Delegate * delegate) override;

    bool Setup() override;
    void Release() override;

    void Start();
    void Cancel();

    void StartScan();
    void CancelScan();
    void ReloadDevices();
    void AdvanceDeviceState(ac::NetworkDeviceState state);
    void TriggerReadyChange();

    void Scan(const std::chrono::seconds &timeout) override;
    bool Connect(const ac::NetworkDevice::Ptr& device) override;
    bool Disconnect(const ac::NetworkDevice::Ptr& device) override;

    std::vector<ac::NetworkDevice::Ptr> Devices() const override;
    ac::IpV4Address LocalAddress() const override;
    bool Running() const override;
    bool Scanning() const override;
    bool Ready() const override;

    void SetCapabilities(const std::vector<Capability> &capabilities);
    std::vector<Capability> Capabilities() const;

    void OnFirmwareLoaded() override;
    void OnFirmwareUnloaded() override;

    void OnManagerReady() override;

    void OnHostnameChanged() override;

    void OnRfkillChanged(const RfkillManager::Type &type) override;

private:
    static void OnServiceLost(GDBusConnection *connection, const gchar *name, gpointer user_data);
    static void OnServiceFound(GDBusConnection *connection, const gchar *name, const gchar *name_owner, gpointer user_data);

    static void OnURfkillAvailable(GDBusConnection*, const gchar*, const gchar*, gpointer user_data);
    static void OnURfkillNotAvailable(GDBusConnection*, const gchar*, gpointer user_data);

private:
    NetworkManager();
    std::shared_ptr<NetworkManager> FinalizeConstruction();

    void FinishRfkillInitialization();

    void Initialize(bool firmware_loading = false);
    void ReleaseInternal();
    void ReleaseInterface();
    void ConfigureFromCapabilities();

    void StartConnectTimeout();
    void StopConnectTimeout();

    DeviceType GenerateWfdDeviceType();

    std::string SelectHostname();
    std::string SelectDeviceType();

    void HandleConnectFailed();

    void OnGroupInterfaceReady();
    void OnManagementInterfaceReady();

    enum class MiracastMode : int {
        kOff = 0,
        kSource = 1,
        kSink = 2
    };

    std::string BuildMiracastModeCommand(MiracastMode mode);

private:
    ac::ScopedGObject<GDBusConnection> connection_;
    ac::NetworkManager::Delegate *delegate_;
    std::shared_ptr<ManagerStub> manager_;
    std::string driver_cmd_iface_;
    guint scan_timeout_;
    guint connect_timeout_;
    w11tng::WiFiFirmwareLoader firmware_loader_;
    std::string dedicated_p2p_interface_;
    bool session_available_;
    std::vector<Capability> capabilities_;
    Hostname1Stub::Ptr hostname_service_;
    RfkillManager::Ptr rfkill_manager_;
    guint urfkill_watch_;
    std::vector<NetworkManagerDevice::Ptr> devices_;
    std::shared_ptr<InformationElementArray> wfd_ies_;
    std::shared_ptr<ac::NetworkDevice> current_device_;
    mutable std::mutex devices_mutex_;

public:
    void OnInterfaceDeviceFound(NMDevice* device);
    void OnInterfaceDeviceLost(NMDevice* device);

    void OnDeviceFound(NMWifiP2PPeer* device);
    void OnDeviceLost(NMWifiP2PPeer* device);

    void OnP2PConnected(GObject* source_object, GAsyncResult* res, gpointer user_data);
    void AchieveDeviceAccess(NMClient* client);

    NMDevice* NmDevice() {
        return nm_device_;
    }

private:
    NMActiveConnection* active_connection_;
    GCancellable* scan_cancellable;
    GCancellable* cancellable;
    NMClient* nm_client;
    NMDevice* nm_device_;
};

} // namespace w11tng

#endif
