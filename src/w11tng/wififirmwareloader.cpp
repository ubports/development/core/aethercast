/*
 * Copyright (C) 2015 Canonical, Ltd.
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3, as published
 * by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranties of
 * MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <chrono>

#include <ac/logger.h>
#include <ac/networkutils.h>
#include <ac/utils.h>

#include "wififirmwareloader.h"

#define WIFI_GET_FW_PATH_P2P 2

namespace w11tng {

WiFiFirmwareLoader::WiFiFirmwareLoader(const std::string &interface_name, Delegate *delegate) :
    interface_name_(interface_name),
    delegate_(delegate),
    reload_timeout_source_(0),
    max_load_retries(20),
    state_(WiFiFirmwareLoaderState::UNLOADED) {
    // FIXME we need to add some change events to wpa to see when the firmware changes
    // so that we can react on this.
}

WiFiFirmwareLoader::~WiFiFirmwareLoader() {
    if (reload_timeout_source_ > 0)
        g_source_remove(reload_timeout_source_);
}

void WiFiFirmwareLoader::SetInterfaceName(const std::string &interface_name) {
    interface_name_ = interface_name;
}

bool WiFiFirmwareLoader::TryLoad() {
    auto conn = g_bus_get_sync(G_BUS_TYPE_SYSTEM, nullptr, nullptr);

    GVariant *params = g_variant_new("(q)", WIFI_GET_FW_PATH_P2P);
    g_dbus_connection_call(conn, "org.halium.mechanicd", "/org/halium/mechanicd/WifiMode",
                           "org.halium.mechanicd.WifiMode", "requestWifiModeSwitch", params,
                           nullptr, (GDBusCallFlags) G_DBUS_CALL_FLAGS_NONE, -1,
                           nullptr, (GAsyncReadyCallback) &WiFiFirmwareLoader::OnInterfaceFirmwareSet, this);

    state_ = WiFiFirmwareLoaderState::LOADING;
    return true;
}

gboolean WiFiFirmwareLoader::OnRetryLoad(gpointer user_data) {
    auto inst = static_cast<WiFiFirmwareLoader*>(user_data);

    if (inst->MaxLoadRetries()-- <= 0 || inst->State() != WiFiFirmwareLoaderState::LOADED)
        return TRUE;

    if (!inst->TryLoad()) {
        return FALSE;
    }

    if (inst->delegate_)
        inst->delegate_->OnFirmwareLoaded();

    return TRUE;
}

void WiFiFirmwareLoader::OnInterfaceFirmwareSet(GDBusConnection *conn, GAsyncResult *res, gpointer user_data) {
    auto inst = static_cast<WiFiFirmwareLoader*>(user_data);
    auto timeout = std::chrono::milliseconds(1000);
    GError *error = nullptr;

    GVariant *result = g_dbus_connection_call_finish(conn, res, &error);
    if (!result) {
        AC_WARNING("Failed to load required WiFi firmware: %s", error->message);
        g_error_free(error);
        timeout = std::chrono::milliseconds(2000);
    }

    inst->reload_timeout_source_ = g_timeout_add(timeout.count(), &WiFiFirmwareLoader::OnRetryLoad, inst);
}

} // namespace w11tng
